﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class AngelRole : RoleBase
    {
        public override void NightAction()
        {

        }

        public override void FirstNightAction()
        {
            //Collect angel data
            Console.WriteLine("Angel wake up!");
            Console.WriteLine("Angel go to sleep! ");
            Console.Write("***Input Angel name: ");
            var angel = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Angel)).First();
            angel.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(angel.Name);
            Console.WriteLine("---");
        }
    }
}
