﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    public class ProtectorRole : RoleBase
    {
        public override void NightAction()
        {


            if (IsActive)
            {
                var InvalidProtect = true;
                Console.WriteLine("Protector wake up! Who do you want to protect?");
                do
                {
                    GlobalContext.protectedName = GlobalContext.ValidateInputAlivePlayer();

                    var protectedPerson = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.protectedName)).First();
                    InvalidProtect = (protectedPerson.protectedRound == 0) || (protectedPerson.protectedRound != 0 && (GlobalContext.roundCount - protectedPerson.protectedRound) != 1) ? false : true;
                    if (InvalidProtect)
                    {
                        Console.Write("Invalid. Input again: ");
                    }
                }
                while (InvalidProtect);

            }
            else
            {
                Console.WriteLine("Protector wake up! Who do you want to protect? (Ask for fun, this person is died.)");
                var foo = Console.ReadLine().ToLower();

            }
            Console.WriteLine("Protector go to sleep! ");
            Console.WriteLine("---");
        }

        public override void FirstNightAction()
        {
            //Protector question
            Console.WriteLine("Protector wake up! Who do you want to protect?");
            GlobalContext.protectedName = GlobalContext.ValidateInputCorrectName();
            Console.WriteLine("Protector go to sleep! ");

            //Collect protector data
            Console.Write("***Input Protector name: ");
            var protector = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Protector)).First();
            protector.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(protector.Name);
         Console.WriteLine("---");

        }


    }
}
