﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    public class ProphetRole : RoleBase
    {


        public override void NightAction()
        {
            if (IsActive)
            {
                Console.WriteLine("Prophet wake up! Who do you want to check? ");
                InputAnswer = GlobalContext.ValidateInputAlivePlayer();

                if (GlobalContext.CheckIsWolf(InputAnswer))
                {
                    Console.WriteLine("Yes, this is a wolf!");
                }
                else
                {
                    Console.WriteLine("No, this is not a wolf!");
                }

            }
            else
            {
                Console.WriteLine("Prophet wake up! Who do you want to check ? ***(Ask for fun, this person is died.)");
                var foo = Console.ReadLine().ToLower();

            }
            Console.WriteLine("Prophet go to sleep!");
            Console.WriteLine("---");
        }

        public override void FirstNightAction()
        {
            //Prophet question
            Console.WriteLine("Prophet wake up! Who do you want to check? ");
            var checkedName = GlobalContext.ValidateInputCorrectName();
            if (GlobalContext.CheckIsWolf(checkedName))
            {
                Console.WriteLine("Yes, this is a wolf!");
            }
            else
            {
                Console.WriteLine("No, this is not a wolf!");
            }

            Console.WriteLine("Prophet go to sleep!");

            //Collect prophet info
            Console.Write("***Input Prophet name: ");
            var prophet = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Prophet)).First();
            prophet.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(prophet.Name);
            Console.WriteLine("---");
        }
    }
}
