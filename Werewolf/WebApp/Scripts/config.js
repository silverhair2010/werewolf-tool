﻿
var Config = {
    players: [
        {
            name: 'Linh',
            mp3: 'LinhDo'
        },
        {
            name: 'Huy',
            mp3: 'HuyVu'
        },
        {
            name: 'Hiền',
            mp3: 'HienNguyen'
        },
        {
            name: 'Huyền',
            mp3: 'HuyenPham'
        },
        {
            name: 'Thịnh',
            mp3: 'Thinh'
        },
        {
            name: 'Sơn Đỗ',
            mp3: 'SonDo'
        },
        {
            name: 'Hiếu',
            mp3: 'HieuXanh'
        },
        {
            name: 'Đức',
            mp3: 'AnhDuc'
        },
        {
            name: 'Phú',
            mp3: 'PhuNguyen'
        },
        {
            name: 'Thạch',
            mp3: 'ThachNguyen'
        },
        {
            name: 'NAnh',
            mp3: 'NinhAnh'
        },
        {
            name: 'Phương',
            mp3: 'PhuongChu'
        },
        {
            name: 'Cường',
            mp3: 'CuongPhan'
        },
        {
            name: 'Khánh',
            mp3: 'KhanhNguyen'
        },
        {
            name: 'Thế',
            mp3: 'TheLe'
        },
        {
            name: 'Hoàng',
            mp3: 'HoangLe'
        },
        {
            name: 'Nguyệt',
            mp3: 'Nguyet'
        },
        {
            name: 'Dương',
            mp3: 'Duong'
        },
        {
            name: 'Hiền(Phú)',
            mp3: 'HienPhu'
        },
        {
            name: 'Chi',
            mp3: 'ThuyChi'
        },
    ],
}