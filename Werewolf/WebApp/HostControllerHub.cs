﻿using Microsoft.AspNet.SignalR;

namespace WebApp
{
    public class HostControllerHub : Hub
    {
        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.All.addNewMessageToPage(name, message);
        }

        public void RandomPlayer()
        {
            Clients.All.randomPlayer();
        }

        public void StartTimer()
        {
            Clients.All.startTimer();
        }

        public void AnnounceDeath(string deathPlayers)
        {
            Clients.All.announceDeath(deathPlayers);
        }

        public void ChangePlayers(string names)
        {
            Clients.All.changePlayers(names);
        }
    }
}