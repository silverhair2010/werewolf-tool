﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class CupidRole : RoleBase
    {
        public override void FirstNightAction()
        {
            //Cupid question
            Console.WriteLine("Cupid wake up! Who will be falled in love?");
            Console.Write("Input 1st person: ");
            GlobalContext.lover1Name = GlobalContext.ValidateInputCorrectName();
            Console.Write("Input 2nd person: ");
            GlobalContext.lover2Name = GlobalContext.ValidateInputCorrectName();
            Console.WriteLine("Cupid go to sleep! ");

            //Collect cupid info
            Console.Write("***Input cupid name: ");
            var cupid = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Cupid)).First();
            cupid.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(cupid.Name);
            Console.WriteLine("---");
        }

        public override void NightAction()
        {

        }
    }
}
