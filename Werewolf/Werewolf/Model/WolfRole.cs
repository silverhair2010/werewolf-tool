﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    public class WolfRole : RoleBase
    {
        public override void NightAction()
        {

            Console.Write("Wolfs wake up! Do do you want to kill someone? (Y/N)");
            GlobalContext.wolfAnswer = Console.ReadLine().ToLower();
            if (GlobalContext.wolfAnswer == "y")
            {
                Console.Write("Input person will be kill: ");
                GlobalContext.killedName = GlobalContext.ValidateInputAlivePlayer();

            }
            Console.WriteLine("Wolfs go to sleep!");
            Console.WriteLine("---");
        }

        public override void FirstNightAction()
        {
            //Wolf question
            Console.Write("Wolfs wake up! Do do you want to kill someone? (Y/N)");
            GlobalContext.wolfAnswer = Console.ReadLine().ToLower();
            if (GlobalContext.wolfAnswer == "y")
            {
                Console.Write("Input person will be kill: ");
                GlobalContext.killedName = GlobalContext.ValidateInputCorrectName();
            }
            Console.WriteLine("Wolfs go to sleep!");

            //Collect wolfs name
            List<Player> wolfs = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Wolf)).ToList();
            for (int i = 0; i < wolfs.Count; i++)
            {
                Console.Write($"***Input wolf {i + 1} name: ");
                wolfs[i].Name = GlobalContext.ValidateInputRoleName();
                GlobalContext.definedName.Add(wolfs[i].Name);
                GlobalContext.wolfsName.Add(wolfs[i].Name);
            }

            Console.WriteLine("---");
        }
    }
}
