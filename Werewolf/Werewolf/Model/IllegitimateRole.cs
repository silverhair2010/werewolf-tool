﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class IllegitimateRole : RoleBase
    {
        public override void FirstNightAction()
        {

            //Illegitimate question
            Console.WriteLine("Illegitimate wake up! Who will be your mum?");
            GlobalContext.mumName = GlobalContext.ValidateInputCorrectName();
            Console.WriteLine("Illegitimate go to sleep! ");

            //Collect Illegitimate info
            Console.Write("***Input Illegitimate name: ");
            var illegitimate = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Illegitimate)).First();
            illegitimate.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(illegitimate.Name);
         Console.WriteLine("---");
        }

        public override void NightAction()
        {

        }
    }
}
