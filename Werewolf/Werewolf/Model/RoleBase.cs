﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Werewolf.Model
{
    public abstract  class RoleBase
    {
        public  int count = 0 ;
        public bool IsActive = true;
        public bool IsDone = false;
        public string InputAnswer = null;
        public abstract void NightAction();
        public abstract void FirstNightAction();
    }
}
