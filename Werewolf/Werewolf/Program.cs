﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Werewolf.Model;

namespace Werewolf
{
    class Program
    {
        static void Main(string[] args)
        {
            GlobalContext.Init();
            Console.WriteLine("Ready?");
            Console.ReadKey();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine($"Night {GlobalContext.roundCount} is starting. All villagers go to sleep zzzzzzzzzzzzzzzzzzzzzzz!");
            Console.WriteLine("---");
            foreach (RoleBase b in GlobalContext.roleList)
            {
                b.FirstNightAction();
            }
            GlobalContext.PostNightAction();
            while (GlobalContext.CheckResult())
            {
                GlobalContext.DayAction();
                GlobalContext.roundCount++;
                if (!GlobalContext.CheckResult()) { break; }
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine($"Night {GlobalContext.roundCount} is starting. All villagers go to sleep zzzzzzzzzzzzzzzzzzzzzzzz!");
                Console.WriteLine("---");
                foreach (RoleBase b in GlobalContext.roleList)
                {
                    b.NightAction();
                }
                GlobalContext.PostNightAction();
            }


        }
    }
}
