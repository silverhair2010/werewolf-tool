﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    public class HunterRole : RoleBase
    {
        public override void NightAction()
        {
            if (IsActive)
            {
                Console.Write("Hunter wake up! Do you want to choose someone to die with you?(Y/N) ");
                var answer = Console.ReadLine().ToLower();
                if (answer == "y")
                {
                    Console.WriteLine("Input person will be died with hunter: ");
                    GlobalContext.diedWithHunterName = GlobalContext.ValidateInputAlivePlayer();
                }
            }
            else
            {
                Console.WriteLine("Hunter wake up! Do you want to choose someone to die with you? ***(Ask for fun, this person is died.)");
                var foo = Console.ReadLine().ToLower();
            }
            Console.WriteLine("Hunter go to sleep!");
            Console.WriteLine("---");
        }

        public override void FirstNightAction()
        {
            //Hunter Question
            Console.Write("Hunter wake up! Do you want to choose someone to die with you? (Y/N) ");
            var answer = Console.ReadLine().ToLower();
            if (answer == "y")
            {
                Console.Write("Input person will be died with hunter: ");
                GlobalContext.diedWithHunterName = GlobalContext.ValidateInputCorrectName();
            }
            Console.WriteLine("Hunter go to sleep!");

            //Collect hunter info
            Console.Write("***Input Hunter name: ");

            var hunter = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Hunter)).First();
            hunter.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(hunter.Name);
            Console.WriteLine("---");
        }
    }
}
