﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class WizardRole : RoleBase
    {
        public bool CanKill = true;
        public bool CanSave = true;
        public override void FirstNightAction()
        {
            //Wizard Question
            if (GlobalContext.curseWolfAnswer != "y")
            {
                Console.WriteLine("Wizard wake up! Killed person is ..., do you want to save? (Y/N) ");
                string answer1 = Console.ReadLine().ToLower();
                if (answer1 == "y")
                {
                    GlobalContext.saveByWizard = true;
                    CanSave = false;
                }
            }
            else
            {
                Console.Write("Wizard wake up! Killed person is ..., do you want to save? (Ask for fun, Cannot save because the victim is curse) ");
                string foo = Console.ReadLine();
            }

            Console.Write("Wizard! Do you want to kill someone? (Y/N) ");
            string answer = Console.ReadLine().ToLower();
            if (answer == "y")
            {
                Console.Write("Input person will be kill: ");
                GlobalContext.killedByWizardName = GlobalContext.ValidateInputCorrectName();
                CanKill = false;
            }

            Console.WriteLine("Wizard go to sleep!");

            //Collect wizard data
            Console.Write("***Input Wizard name: ");
                        var wizard = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Wizard)).First();
            wizard.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(wizard.Name);
         Console.WriteLine("---");
        }

        public override void NightAction()

        {
            if (CanSave && IsActive)
            {
                if (GlobalContext.wolfAnswer == "y" && GlobalContext.curseWolfAnswer != "y")
                {
                    Console.WriteLine("Wizard wake up! Killed person is ..., do you want to save? (Y/N) ");
                    string answer1 = Console.ReadLine().ToLower();
                    if (answer1 == "y")
                    {
                        GlobalContext.saveByWizard = true;
                        CanSave = false;
                    }
                }
                else
                {
                    Console.Write("Wizard wake up! Killed person is ..., do you want to save? (Ask for fun, Cannot save because the victim is curse or no victim) ");
                    string foo = Console.ReadLine();
                }
            }
            else
            {
                Console.Write("Wizard wake up! Killed person is ..., do you want to save? (Ask for fun, Cannot save anymore) ");
                string foo = Console.ReadLine();
            }

            if (CanKill && IsActive)
            {
                Console.Write("Wizard! Do you want to kill someone? (Y/N) ");
                string answer = Console.ReadLine().ToLower();
                if (answer == "y")
                {
                    Console.Write("Input person will be killed: ");
                       GlobalContext.killedByWizardName = GlobalContext.ValidateInputAlivePlayer();
                    CanKill = false;
                }
            }
            else
            {
                Console.Write("Wizard! Do you want to kill someone? (Y/N) (Ask for fun, cannot kill anymore) ");
                string foo = Console.ReadLine();
            }
            Console.WriteLine("Wizard go to sleep!");
            Console.WriteLine("---");
        }
    }
}
