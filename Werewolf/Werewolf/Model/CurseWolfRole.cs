﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class CurseWolfRole : RoleBase
    {
        public override void NightAction()
        {
            if (IsDone || !IsActive)
            {
                Console.WriteLine("CurseWolf! Do you want to change this person to wolf? (Ask for fun, cannot curse anymore ) ");
                var foo = Console.ReadLine().ToLower();
            }
            else if (GlobalContext.wolfAnswer != "y")
            {
                Console.WriteLine("CurseWolf! Do you want to change this person to wolf? (Ask for fun, wolfs do not kill anyone) ");
                var foo = Console.ReadLine().ToLower();
            }
            else
            {
                Console.Write("CurseWolf! Do you want to change this person to wolf? (Y/N): ");
                GlobalContext.curseWolfAnswer = Console.ReadLine().ToLower();
            }
            Console.WriteLine("CurseWolf go to sleep");
            Console.WriteLine("---");
        }
        public override void FirstNightAction()
        {
            //CurseWolf Question
            string cursewolfName;
            if (GlobalContext.wolfAnswer != "y")
            {
                Console.WriteLine("CurseWolf! Do you want to change this person to wolf? (Ask for fun, wolfs do not kill anyone) ");
                var foo = Console.ReadLine().ToLower();
            }
            else
            {
                Console.WriteLine("CurseWolf! Do you want to change this person to wolf? (Y/N)");
                GlobalContext.curseWolfAnswer = Console.ReadLine().ToLower();
            }
            Console.WriteLine("CurseWolf go to sleep");

            //Collect CurseWolf info
            Console.Write($"***Input crusewolf name: ");
            cursewolfName = GlobalContext.ValidateCurseWolfName();
            var crusewolf = GlobalContext.players.FirstOrDefault(p => p.Name != null && p.Name.Equals(cursewolfName));
            crusewolf.Role = RoleEnum.CurseWolf;
            Console.WriteLine("---");
        }
    }
}
