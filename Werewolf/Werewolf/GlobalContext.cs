﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Werewolf.Model;
using static Werewolf.Player;

namespace Werewolf
{
    public static class GlobalContext
    {
        public static int roundCount = 1;


        public static RoleBase protectorRole = new ProtectorRole();
        public static RoleBase wolfRole = new WolfRole();
        public static RoleBase hunterRole = new HunterRole();
        public static RoleBase farmerRole = new FarmerRole();
        public static RoleBase curseWolfRole = new CurseWolfRole();
        public static RoleBase prophetRole = new ProphetRole();
        public static RoleBase angelRole = new AngelRole();
        public static RoleBase cupidRole = new CupidRole();
        public static RoleBase wizardRole = new WizardRole();
        public static RoleBase osinRole = new OsinRole();
        public static RoleBase illegitimateRole = new IllegitimateRole();

        public static List<RoleBase> roleList = new List<RoleBase> { cupidRole, osinRole, illegitimateRole, protectorRole, hunterRole, wolfRole, curseWolfRole, wizardRole, prophetRole, angelRole, farmerRole };
        public static List<Player> players = new List<Player>();
        public static List<Player> diedPlayers = new List<Player>();
        public static List<string> aliveName = new List<string>();
        public static List<string> playersName = new List<string>();
        public static List<string> wolfsName = new List<string>();
        public static List<string> definedName = new List<string>();

        public static int TotalPlayer;
        public static string protectedName;
        public static string killedName;
        public static string diedWithHunterName;
        // public static string hungName;
        public static string curseWolfAnswer;
        public static string wolfAnswer;
        public static string lover1Name;
        public static string lover2Name;
        public static bool saveByWizard;
        public static string killedByWizardName;
        public static bool angelDieByLoveInMorning;
        public static string bossName;
        public static RoleEnum bossRole;
        public static string mumName;

        public static int TotalHuman { get { return farmerRole.count + hunterRole.count + protectorRole.count + prophetRole.count + angelRole.count + cupidRole.count + wizardRole.count + osinRole.count + illegitimateRole.count; } }
        public static int TotalWolf { get { return wolfRole.count + curseWolfRole.count; } }

        public static bool IsHumanWin;
        public static bool IsWolfWin;
        public static bool IsAngelWin;

        public static bool IsContinue
        {
            get
            {
                if (IsAngelWin)
                {
                    return false;
                }
                else if (TotalWolf == 0)
                {
                    IsHumanWin = true;
                    return false;
                }
                else if (TotalWolf >= TotalHuman)
                {
                    IsWolfWin = true;
                    return false;
                }
                return true;
            }
        }

        public static bool CheckIsWolf(string checkedName)
        {
            var wolfs = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Wolf));
            var curseWolfs = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.CurseWolf));
            var curseToBeWolfs = GlobalContext.players.Where(p => p.IsCurseToBeWolf.Equals(true));
            List<string> allWolfName = new List<string>();
            foreach (Player p in wolfs)
            {
                allWolfName.Add(p.Name);
            }
            foreach (Player p in curseWolfs)
            {
                allWolfName.Add(p.Name);
            }
            foreach (Player p in curseToBeWolfs)
            {
                allWolfName.Add(p.Name);
            }
            return (allWolfName.Contains(checkedName)) ? true : false;
        }
        public static int ValidateInputGreaterThan7()
        {

            var inputNumber = int.Parse(Console.ReadLine());
            while (inputNumber < 8)
            {
                Console.Write("Must be greater than 7. Input again:");
                inputNumber = int.Parse(Console.ReadLine());
            }
            return inputNumber;
        }

        public static int ValidateInputLessThan2()
        {

            var inputNumber = int.Parse(Console.ReadLine());
            while (inputNumber > 1 || inputNumber < 0)
            {
                Console.Write("Must be 0 or 1. Input again:");
                inputNumber = int.Parse(Console.ReadLine());
            }
            return inputNumber;
        }

        public static string ValidateInputRoleName()
        {
            var inputName = Console.ReadLine().ToLower();
            while (!playersName.Contains(inputName) || definedName.Contains(inputName))
            {
                Console.Write("Incorrect. Input again:");
                inputName = Console.ReadLine().ToLower();
            }
            return inputName;
        }
        public static string ValidateInputCorrectName()
        {
            var inputName = Console.ReadLine().ToLower();
            while (!playersName.Contains(inputName))
            {
                Console.Write("Incorrect. Input again:");
                inputName = Console.ReadLine().ToLower();
            }
            return inputName;
        }
        public static string ValidateCurseWolfName()
        {
            var inputName = Console.ReadLine().ToLower();
            while (!wolfsName.Contains(inputName))
            {
                Console.Write("Incorrect. Input again:");
                inputName = Console.ReadLine().ToLower();
            }
            return inputName;
        }
        public static string ValidateInputAlivePlayer()
        {
            var inputName = Console.ReadLine().ToLower();
            while (!GlobalContext.aliveName.Contains(inputName))
            {
                Console.Write("Incorrect. Input again:");
                inputName = Console.ReadLine().ToLower();
            }
            return inputName;
        }

        public static string ValidateUniquePlayerName()
        {
            var inputName = Console.ReadLine().ToLower();
            while (playersName.Contains(inputName))
            {
                Console.Write("Player name must be unique. Input again: ");
                inputName = Console.ReadLine().ToLower();
            }

            return inputName;
        }

        public static void Init()
        {
            Console.Write("Input total player:");
            TotalPlayer = ValidateInputGreaterThan7();

            switch (TotalPlayer)
            {

                case 8:
                    Console.WriteLine("Farmer:1 | Wolf:1 |CurseWolf: 1| Hunter:1 | Protector:1 | Prophet:1 | Angel:1 | Cupid:1");
                    farmerRole.count = 1;
                    wolfRole.count = 1;
                    curseWolfRole.count = 1;
                    hunterRole.count = 1;
                    protectorRole.count = 1;
                    prophetRole.count = 1;
                    angelRole.count = 1;
                    cupidRole.count = 1;

                    break;
                case 9:
                    Console.WriteLine("Farmer:1 | Wolf:1 |CurseWolf: 1| Hunter:1 | Protector:1 | Prophet:1 | Angel:1 | Cupid:1 | Wizard:1");
                    farmerRole.count = 1;
                    wolfRole.count = 1;
                    curseWolfRole.count = 1;
                    hunterRole.count = 1;
                    protectorRole.count = 1;
                    prophetRole.count = 1;
                    angelRole.count = 1;
                    cupidRole.count = 1;
                    wizardRole.count = 1;

                    break;
                case 10:
                    Console.WriteLine("Farmer:1 | Wolf:1 |CurseWolf: 1| Hunter:1 | Protector:1 | Prophet:1 | Angel:1 | Cupid:1 | Wizard:1 | Osin:1");
                    farmerRole.count = 1;
                    wolfRole.count = 1;
                    curseWolfRole.count = 1;
                    hunterRole.count = 1;
                    protectorRole.count = 1;
                    prophetRole.count = 1;
                    angelRole.count = 1;
                    cupidRole.count = 1;
                    wizardRole.count = 1;
                    osinRole.count = 1;

                    break;
                case 11:
                    Console.WriteLine("Farmer:1 | Wolf:1 |CurseWolf: 1| Hunter:1 | Protector:1 | Prophet:1 | Angel:1 | Cupid:1 | Wizard:1 | Osin:1 | Illegitimate:1");
                    farmerRole.count = 1;
                    wolfRole.count = 1;
                    curseWolfRole.count = 1;
                    hunterRole.count = 1;
                    protectorRole.count = 1;
                    prophetRole.count = 1;
                    angelRole.count = 1;
                    cupidRole.count = 1;
                    wizardRole.count = 1;
                    osinRole.count = 1;
                    illegitimateRole.count = 1;

                    break;
                default:
                    Console.WriteLine($"Farmer:{TotalPlayer - 10} | Wolf:1 |CurseWolf: 1| Hunter:1 | Protector:1 | Prophet:1 | Angel:1 | Cupid:1 | Wizard:1 | Osin:1 | Illegitimate:1");
                    farmerRole.count = TotalPlayer - 10;
                    wolfRole.count = 1;
                    curseWolfRole.count = 1;
                    hunterRole.count = 1;
                    protectorRole.count = 1;
                    prophetRole.count = 1;
                    angelRole.count = 1;
                    cupidRole.count = 1;
                    wizardRole.count = 1;
                    osinRole.count = 1;
                    illegitimateRole.count = 1;
                    break;
            }

            Console.Write("Do you want to change this default setting? (Y/N)");
            var inputAnswer = Console.ReadLine().ToLower();
            if (inputAnswer == "y")
            {
                do
                {
                    Console.WriteLine($"Input number of players for each role (total roles must be equal {TotalPlayer})");

                    Console.Write("Farmer: ");
                    farmerRole.count = int.Parse(Console.ReadLine());

                    Console.Write("Wolf: ");
                    wolfRole.count = int.Parse(Console.ReadLine());

                    Console.Write("CurseWolf: ");
                    curseWolfRole.count = ValidateInputLessThan2();

                    Console.Write("Hunter: ");
                    hunterRole.count = ValidateInputLessThan2();

                    Console.Write("Protector: ");
                    protectorRole.count = ValidateInputLessThan2();

                    Console.Write("Prophet: ");
                    prophetRole.count = ValidateInputLessThan2();

                    Console.Write("Angel: ");
                    angelRole.count = ValidateInputLessThan2();

                    Console.Write("Cupid: ");
                    cupidRole.count = ValidateInputLessThan2();

                    Console.Write("Wizard: ");
                    wizardRole.count = ValidateInputLessThan2();

                    Console.Write("Osin: ");
                    osinRole.count = ValidateInputLessThan2();

                    Console.Write("Illegitimate: ");
                    illegitimateRole.count = ValidateInputLessThan2();
                }
                while (TotalPlayer != farmerRole.count + wolfRole.count + curseWolfRole.count + hunterRole.count + protectorRole.count + prophetRole.count + angelRole.count + cupidRole.count + wizardRole.count + osinRole.count + illegitimateRole.count);
            }
            for (int i = roleList.Count - 1; i >= 0; i--)
            {
                if (roleList[i].count == 0)
                {
                    if(roleList[i] == wolfRole)
                    {
                        continue;
                    }
                    roleList.RemoveAt(i);
                }

            }
            for (int i = 0; i < farmerRole.count; i++) { players.Add(new Player(RoleEnum.Farmer)); }
            for (int i = 0; i < wolfRole.count + curseWolfRole.count; i++) { players.Add(new Player(RoleEnum.Wolf)); }
            for (int i = 0; i < hunterRole.count; i++) { players.Add(new Player(RoleEnum.Hunter)); }
            for (int i = 0; i < protectorRole.count; i++) { players.Add(new Player(RoleEnum.Protector)); }
            for (int i = 0; i < prophetRole.count; i++) { players.Add(new Player(RoleEnum.Prophet)); }
            for (int i = 0; i < angelRole.count; i++) { players.Add(new Player(RoleEnum.Angel)); }
            for (int i = 0; i < cupidRole.count; i++) { players.Add(new Player(RoleEnum.Cupid)); }
            for (int i = 0; i < wizardRole.count; i++) { players.Add(new Player(RoleEnum.Wizard)); }
            for (int i = 0; i < osinRole.count; i++) { players.Add(new Player(RoleEnum.Osin)); }
            for (int i = 0; i < illegitimateRole.count; i++) { players.Add(new Player(RoleEnum.Illegitimate)); }

            Console.WriteLine("Input all players name");
            for (int i = 1; i < TotalPlayer + 1; i++)
            {
                Console.Write($"Player{i}: ");
                var playerName = ValidateUniquePlayerName();
                playersName.Add(playerName);
            }
        }

        //Process after night
        public static void PostNightAction()
        {
            //setup Boss original role


            //setup lover and boss original role
            if (roundCount == 1)
            {
                if (lover1Name != null && lover2Name != null)
                {
                    var lover1 = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.lover1Name)).First();
                    lover1.LoverName = lover2Name;
                    var lover2 = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.lover2Name)).First();
                    lover2.LoverName = lover1Name;
                }

                if (bossName != null)
                {
                    var boss = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.bossName)).First();
                    bossRole = boss.Role;
                }
            }

            //setup protect by protector
            if (protectedName != null)
            {
                var protectedPerson = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.protectedName)).First();
                protectedPerson.protectedRound = GlobalContext.roundCount;
            }

            //Kill activity
            if (killedName != null)
            {
                var killedPerson = GlobalContext.players.Where(p => p.Name.Equals(killedName)).First();
                if (killedPerson.Name != protectedName)
                {
                    if (curseWolfAnswer == "y")
                    {
                        killedPerson.IsCurseToBeWolf = true;
                        curseWolfRole.IsDone = true;
                        switch (killedPerson.Role)
                        {
                            case RoleEnum.Farmer:
                                farmerRole.count--;
                                break;
                            case RoleEnum.Hunter:
                                hunterRole.count--;
                                break;
                            case RoleEnum.Protector:
                                protectorRole.count--;
                                break;
                            case RoleEnum.Wolf:
                                wolfRole.count--;
                                break;
                            case RoleEnum.CurseWolf:
                                curseWolfRole.count--;
                                break;
                            case RoleEnum.Angel:
                                angelRole.count--;
                                break;
                            case RoleEnum.Cupid:
                                cupidRole.count--;
                                break;
                            case RoleEnum.Osin:
                                osinRole.count--;
                                break;
                            case RoleEnum.Prophet:
                                prophetRole.count--;
                                break;
                            case RoleEnum.Wizard:
                                wizardRole.count--;
                                break;
                            case RoleEnum.Illegitimate:
                                illegitimateRole.count--;
                                break;
                        }
                        wolfRole.count++;
                    }
                    else if (saveByWizard)
                    {

                    }
                    else
                    {
                        killedPerson.IsKill();
                    }
                }
            }

            if (killedByWizardName != null)
            {
                var killedPerson = GlobalContext.players.Where(p => p.Name.Equals(killedByWizardName)).First();
                killedPerson.IsKill();
            }
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("All villager wake up!");
            Console.WriteLine($"Information after night {roundCount}:");
            Console.WriteLine($"Total died person this night is {diedPlayers.Count}");
            foreach (Player p in diedPlayers)
            {
                Console.WriteLine(p.Name);
            }

            //Reset data
            diedPlayers.Clear();
            protectedName = null;
            diedWithHunterName = null;
            killedName = null;
            // hungName = null;
            curseWolfAnswer = null;
            wolfAnswer = null;
            saveByWizard = false;
            killedByWizardName = null;
        }

        /// <summary>
        /// Day Action
        /// </summary>
        public static void DayAction()
        {

            Console.Write("Do you want to hang anyone? (Y/N) ");
            var userChoice = Console.ReadLine().ToLower();
            if (userChoice == "y")
            {
                Console.WriteLine("Who will be hang? ");
                var hungName = ValidateInputAlivePlayer();
                var hungPerson = GlobalContext.players.Where(p => p.Name.Equals(hungName)).First();
                hungPerson.IsHung();
            }
        }
        public static bool CheckResult()

        {
            aliveName.Clear();
            Console.Write("Alive person: ");
            foreach (Player p in players)
            {
                Console.Write(p.IsCurseToBeWolf ? $"{p.Name}({p.Role} + Wolf) |" : $"{p.Name}({p.Role}) |");
                aliveName.Add(p.Name);
            }

            Console.WriteLine("");
            if (IsContinue == false)
            {
                if (IsAngelWin)
                {
                    Console.WriteLine("Angel win!");
                }
                if (IsHumanWin)
                {
                    Console.WriteLine("Human win!");
                }
                if (IsWolfWin)
                {
                    if (players.Count == 2 && players[0].LoverName != null)
                    {
                        Console.WriteLine("Lovers win!");
                    }
                    else
                    {
                        Console.WriteLine("Wolf win!");
                    }
                }
                Console.ReadKey();

            }

            return IsContinue;
        }
    }
}
