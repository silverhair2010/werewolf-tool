﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    class OsinRole : RoleBase
    {
        public override void FirstNightAction()
        {
            //Osin question
            Console.WriteLine("Osin wake up! Who will be your boss?");
            GlobalContext.bossName = GlobalContext.ValidateInputCorrectName();
            Console.WriteLine("Osin go to sleep! ");

            //Collect osin info
            Console.Write("***Input osin name: ");
            var osin = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Osin)).First();
            osin.Name = GlobalContext.ValidateInputRoleName();
            GlobalContext.definedName.Add(osin.Name);
         Console.WriteLine("---");
        }

        public override void NightAction()
        {

        }
    }
}
