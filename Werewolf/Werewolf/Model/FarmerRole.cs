﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Werewolf.Player;

namespace Werewolf.Model
{
    public class FarmerRole : RoleBase
    {
        public override void NightAction()
        {

        }

        public override void FirstNightAction()
        {
           
            foreach (string name in GlobalContext.definedName)
            {
                GlobalContext.playersName.Remove(name);
            }

            List<Player> farmers = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Farmer)).ToList();
            for (int i = 0; i < farmers.Count; i++)
            {

                farmers[i].Name = GlobalContext.playersName[i];
            }

        }
    }
}
