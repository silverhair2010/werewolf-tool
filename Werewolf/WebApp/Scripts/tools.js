﻿// Change background 
function changeBackground() {
    var backgrounds = [
        // // 'url(content/images/card1.jpg) center center',
        // // 'url(content/images/card2.jpg) center center',
        // // 'url(content/images/card3.jpg) center center',
        // // 'url(content/images/card4.jpg) center center',
        // // 'url(content/images/card5.jpg) center center',
        // // 'url(content/images/card6.jpg) center center',
        // // 'url(content/images/card7.jpg) center center',
        // // 'url(content/images/card8.jpg) center center',
        // // 'url(content/images/card9.jpg) center center',
        'url(/Content/images/background1.jpg) no-repeat center center fixed',
        'url(/Content/images/background2.jpg) no-repeat center center fixed',
        'url(/Content/images/background3.jpg) no-repeat center center fixed',
        'url(/Content/images/background4.jpg) no-repeat center center fixed',
        'url(/Content/images/background5.jpg) no-repeat center center fixed',
        'url(/Content/images/background6.jpg) no-repeat center center fixed',
        'url(/Content/images/background7.jpg) no-repeat center center fixed',
        'url(/Content/images/background8.jpg) no-repeat center center fixed',
        'url(/Content/images/background9.jpg) no-repeat center center fixed',
        'url(/Content/images/background10.jpg) no-repeat center center fixed',
        'url(/Content/images/background11.jpg) no-repeat center center fixed',
        'url(/Content/images/background12.jpg) no-repeat center center fixed',
        'url(/Content/images/background13.jpg) no-repeat center center fixed',
        'url(/Content/images/background14.jpg) no-repeat center center fixed',
    ];
    var random = Math.floor(Math.random() * backgrounds.length);

    $('#background').css('background', backgrounds[random]);
    setTimeout(changeBackground, 30000);
}
setTimeout(changeBackground, 30000);

function randomMusic() {
    var musics = [
        'https://www.youtube.com/embed/mlLGwW7eHTI',
        'https://www.youtube.com/embed/AtGTot98UAo',
        'https://www.youtube.com/embed/lPUTddT8APc',
        'https://www.youtube.com/embed/4bhceZgdWFM',
        'https://www.youtube.com/embed/WWUHh9t7Tj0',
        'https://www.youtube.com/embed/NdCdLg0741E',
        'https://www.youtube.com/embed/jWObTfiovD8',
        'https://www.youtube.com/embed/Egyc7mYh0CA',
    ]
    var random = Math.floor(Math.random() * musics.length);
    $('#musicarea').html($('<iframe width="1" height="1" src="' + musics[random] + '?autoplay=1" frameborder="0" allowfullscreen></iframe>'));
}
//randomMusic();






// Random list
//            https://stackoverflow.com/questions/26139144/making-groups-with-random-names-in-it-in-javascript
//            http://jsfiddle.net/518b000u/1/
function textSpliter(group) {
    var input = document.getElementById("listext").value;
    var names = input.split(",");

    // var groupSize = document.getElementById("groupNumber").value;
    var groupSize = group;
    var groups = textSplit(names, groupSize);
     
    printGroups(groups);
}

function textSplit(input, groupSize) {
    var groupCount = Math.ceil(input.length / groupSize);
    var groups = [];

    for (var i = 0; i < groupCount; i++) {
        var group = [];
        for (var j = 0; j < groupSize; j++) {
            var random = Math.floor(Math.random() * input.length);
            var name = input[random];
            if (name != undefined) {
                group.push(name);
                input.splice(input.indexOf(name), 1);
            }
        }
        group.sort();
        groups.push(group);
    }
    return groups;
}

function printGroups(group) {
    var output = document.getElementById("resInput");
    output.value = "";
    for (var i = 0; i < group.length; i++) {
        var currentGroup = "";
        for (var j = 0; j < group[i].length; j++) {
            currentGroup = group[i].join(",");
        }
        output.value += currentGroup + "\r";
    }
}



/* EVENTS */
function RemoveVietnameseCharacter(str) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    return str;
}

// Event search
$('#eventSearch').keyup(function () {
    var keyword = $('#eventSearch').val().toLowerCase();
    $('#events .col-sm-12').addClass('hide');
    $('#events .col-sm-12').each(function (i) {
        if (RemoveVietnameseCharacter($('h4', this).text()).indexOf(keyword) >= 0) {
            $(this).removeClass('hide');
        }
    });
});
// Open event modal
$('#events button').click(function () {
    var modal = $('#myModal');
    var p = $(this).closest(".col-sm-12");
    $('.modal-title', modal).text($('h4', p).text());

    $('.modal-body', modal).html($('.alert', p).html());
    modal.modal('show');
});

//Gamble
function Gamble() {
    var coin = $('#coin');
    if (coin.hasClass('animate')) {
        $('#btnGambleStop').click();
    } else {
        $('#btnGamble').click();
    }
}
function GambleStart() {
    var coin = $('#coin');

    coin.removeClass('up').removeClass('down').addClass('animate');
};
function GambleStop() {
    var coin = $('#coin');

    coin.removeClass('up').removeClass('down');
    var random = Math.floor(Math.random() * (2 - 1 + 1) + 1); //https://stackoverflow.com/questions/22363616/generate-random-number-between-2-variables-jquery
    if (random == 1) {
        coin.addClass('down');
    } else {
        coin.addClass('up');
    }
    coin.delay(800).removeClass('animate').fadeIn(1000);
};

function playerSplitter() {
    var input = document.getElementById("listext").value;
    var names = input.split(",");

    var random = Math.floor(Math.random() * names.length);
    $('#TheLittleRascal').text(names[random]);

    var senior = textSplit(names, names.length / 2);
    $('#TheSeniors').text(senior[0]);
    $('#TheYoungers').text(senior[1]);
    
    var names = input.split(",");
    var sex = textSplit(names, names.length / 2);
    $('#TheMen').text(sex[0]);
    $('#TheWomen').text(sex[1]);

    // $('#btnPlayerSpliter').attr('disabled', 'disabled');
}

$('#events button').each(function () {
    $(this).addClass('hide');
});

// select-unselect all players
function checkAll() {
    $('input[name=players]').each(function () {
        $(this)[0].checked = !_checkedAll;
    });
    changePlayer();
    _checkedAll = !_checkedAll;
}

function resetdeathText() {
    $('#deathText').val('');
}

function showPlayers(names) {
    $('#chkPlayers div').each(function (i, e) {
        $(this).removeClass('hide');
        var v = this.innerText.trim();
        if (names.indexOf(v) < 0) {
            $(this).addClass('hide');
        }
    });
}

// update players list by check/uncheck checkboxes
function changePlayer(e) {
    if (e && !$(e.target)[0].checked) {
        _checkedAll = false;
    }
    var arr = [];
    $('input:checked[name=players]').each(function () {
        arr.push($(this).val());
    });

    var names = arr.join(',');

    if ($('#choosePlayers')) {
        var text = '<i class="glyphicon glyphicon-ok" aria-hidden="true"></i> Chọn người chơi ({0})';
        text = text.replace('{0}', arr.length);
        $('#choosePlayers').html(text);
    }

    if ($('#listext')) {
        $('#listext').val(names);
    }

    if ($('#deathText').val()) {
        $('#deathText').val($('#deathText').val() + ',' + e.currentTarget.title);
    }
    else {
        $('#deathText').val(e.currentTarget.title);
    }

   
};

function announceDeath(names) {
    names = names || $('#deathText').val();
    var namesArr = [];
    var announcer = null;
    var steps = [];
    if(names)
        namesArr = names.split(',');
    var textToSpeech = '';

    if (namesArr.length == 0) {
        steps = [
            {
                file: 'DemQuaKhongCoAiChet',
            }
        ];
    }
    else {    
        steps = [
            {
                file: 'DemQuaCo{0}NguoiChet'.format(namesArr.length),
            },
            {
                file: 'NguoiChetDemQuaLa',
            },
        ];

        namesArr.forEach(function (name) {
            var player = Config.players.find(function (p) { return p.name == name.trim() });
            if (player) {
                steps.push({
                    file: player.mp3,
                });
            }
        });

        steps.push({
            file: 'MoiAnhChiDiRa',
        });

    }

    announcer = new Announcer(steps);
    announcer.play();
}



// "{0} is {1}".format("Son", "Wolf");
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
            ;
        });
    };
}
//var players = "Linh,Huy,Hiền,Huyền,Sơn Đỗ,Hiếu,Đức,Phú,Thạch,NAnh,Phương,Cường,Khánh,Thế,Hoàng,Nguyệt,Dương,Hiền(Phú),Chi";
//var playersArr = players.split(",");
var tmpl = `<div class="col-xs-4">
            <input type="checkbox" id="players{0}" name="players" value="{0}" title="{0}" onchange="changePlayer(event)" checked />
            <label for="players{0}" class="red strong">{0}</label>
            </div>`;

$(document).ready(function () {
    Config.players.forEach(function (player) {
        var html = tmpl.format(player.name);
        $(html).appendTo("#chkPlayers");
    });
});


