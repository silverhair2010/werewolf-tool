﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Werewolf.Model;

namespace Werewolf
{
    public class Player
    {
        public string Name { get; set; }
        public string LoverName { get; set; }
        public bool IsAlive { get; set; }
        public bool IsCurseToBeWolf = false;
        public int protectedRound = 0;


        public RoleEnum Role { get; set; }
        public Player(RoleEnum role)
        {
            Role = role;
        }

        public void IsDie()
        {
            IsAlive = false;
                      
            switch (Role)
            {
                case RoleEnum.Angel:
                    if (GlobalContext.roundCount == 1)
                    {
                        GlobalContext.IsAngelWin = true;
                    }

                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.angelRole.count--;
                    }
                    GlobalContext.angelRole.IsActive = false;

                    if (GlobalContext.angelDieByLoveInMorning)
                    {
                        GlobalContext.IsAngelWin = false;
                    }

                    break;
                case RoleEnum.Farmer:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.farmerRole.count--;
                    }
                    break;
                case RoleEnum.Protector:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.protectorRole.count--;
                    }
                    GlobalContext.protectorRole.IsActive = false;
                    break;
                case RoleEnum.Hunter:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.hunterRole.count--;
                    }
                    GlobalContext.hunterRole.IsActive = false;
                    break;
                case RoleEnum.Prophet:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.prophetRole.count--;
                    }
                    GlobalContext.prophetRole.IsActive = false;
                    break;

                case RoleEnum.Wolf:
                    GlobalContext.wolfRole.count--;
                    break;

                case RoleEnum.CurseWolf:
                    GlobalContext.curseWolfRole.count--;
                    GlobalContext.curseWolfRole.IsActive = false;
                    break;
                case RoleEnum.Cupid:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.cupidRole.count--;
                    }
                    break;
                case RoleEnum.Wizard:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.wizardRole.count--;
                    }
                    GlobalContext.wizardRole.IsActive = false;
                    break;
                case RoleEnum.Osin:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.osinRole.count--;
                    }
                    break;
                case RoleEnum.Illegitimate:
                    if (this.IsCurseToBeWolf)
                    {
                        GlobalContext.wolfRole.count--;
                    }
                    else
                    {
                        GlobalContext.illegitimateRole.count--;
                    }
                    break;
            }
            GlobalContext.players.Remove(this);
            GlobalContext.diedPlayers.Add(this);

            //check if die person is a boss, his osin will inherit his role
            if (this.Name == GlobalContext.bossName)
            {
                var osin = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Osin)).First();
                osin.Role = GlobalContext.bossRole;
                GlobalContext.osinRole.count--;
                switch (GlobalContext.bossRole)
                {
                    case RoleEnum.Farmer:
                        GlobalContext.farmerRole.count++;
                        break;
                    case RoleEnum.Hunter:
                        GlobalContext.hunterRole.count++;
                        GlobalContext.hunterRole.IsActive = true;
                        break;
                    case RoleEnum.Protector:
                        GlobalContext.protectorRole.count++;
                        GlobalContext.protectorRole.IsActive = true;
                        break;
                    case RoleEnum.Wolf:
                        GlobalContext.wolfRole.count++;
                        break;
                    case RoleEnum.CurseWolf:
                        GlobalContext.curseWolfRole.count++;
                        break;
                    case RoleEnum.Angel:
                        GlobalContext.angelRole.count++;
                        break;
                    case RoleEnum.Cupid:
                        GlobalContext.cupidRole.count++;
                        break;
                    case RoleEnum.Prophet:
                        GlobalContext.prophetRole.count++;
                        GlobalContext.prophetRole.IsActive = true;
                        break;
                    case RoleEnum.Wizard:
                        GlobalContext.wizardRole.count++;
                        GlobalContext.wizardRole.IsActive = true;
                        break;
                    case RoleEnum.Illegitimate:
                        GlobalContext.illegitimateRole.count++;
                        break;
                }
            }

            //check if die person is a mom, his son will become wolf
            if (this.Name == GlobalContext.mumName)
            {
                var illigitimate = GlobalContext.players.Where(p => p.Role.Equals(RoleEnum.Illegitimate)).First();
                illigitimate.Role = RoleEnum.Wolf;
                GlobalContext.illegitimateRole.count--;
                GlobalContext.wolfRole.count++;

            }
        }
        public void IsKill()
        {

            IsDie();

            //Check if diedPerson is Hunter
            if (this.Role == RoleEnum.Hunter && GlobalContext.diedWithHunterName != null)
            {
                var dieWithHunterPerson = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.diedWithHunterName)).First();
                dieWithHunterPerson.IsDie();

                //Check if diedWithHunterPerson have lover
                if (dieWithHunterPerson.LoverName != null)
                {
                    var lover = GlobalContext.players.Where(p => p.Name.Equals(dieWithHunterPerson.LoverName)).First();
                    lover.IsDie();
                }

            }

            //Check if diedPerson have lover
            if (this.LoverName != null)
            {
                var lover = GlobalContext.players.Where(p => p.Name.Equals(this.LoverName)).First();
                lover.IsDie();


                //Check if lover is hunter
                if (lover.Role == RoleEnum.Hunter && GlobalContext.diedWithHunterName != null)
                {
                    var dieWithHunterPerson = GlobalContext.players.Where(p => p.Name.Equals(GlobalContext.diedWithHunterName)).First();
                    dieWithHunterPerson.IsDie();
                }
            }
        }



        public void IsHung()
        {

            IsDie();
            GlobalContext.diedPlayers.Remove(this);

            //Check if diedPerson have lover
            if (this.LoverName != null)
            {
                var lover = GlobalContext.players.Where(p => p.Name.Equals(this.LoverName)).First();
                if (GlobalContext.roundCount == 1 & lover.Role == RoleEnum.Angel)
                {
                    GlobalContext.angelDieByLoveInMorning = true;
                }
                lover.IsDie();
            }
        }

        public enum RoleEnum { Farmer, Hunter, Protector, Wolf, CurseWolf, Prophet, Angel, Cupid, Wizard, Osin, Illegitimate }
    }
}
